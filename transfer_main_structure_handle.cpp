#include<string>

//TransferHandler handle the process and status of uploading and downloading
class TransferHandler{
public:

    //current upload status: file in uploading, file upload completed, file upload failed   
    void GetUploadProgress(){
        //pesudo code
        if(file in uploading) 
        {
            show CompletedUploadQueue.size()/PartCount;
        }    
        else if (file upload completed)
        {}
        else
        {}
    }
    /**
 * Reset the cancellation status for a retry. This will be done automatically by Transfermanager.
 */
    void Restart();

/**
* Returns false if Cancel has been called. Largely for internal use.
*/
    bool ShouldContinue() const;

/**
 * Cancel the transfer. This will happen asynchronously, so if you need to wait for it to be canceled, either handle the callbacks,
 *  or call WaitUntilFinished.
 */
    void Cancel();

/**
 * Total bytes transferred successfully on this transfer operation.
 */
    inline uint64_t GetBytesTransferred() const {}

/**
* Total bytes transferred successfully on this transfer operation.
*/
    void UpdateBytesTransferred(uint64_t amount) { m_bytesTransferred += amount; }

/**
 * The calculated total size of the object being transferred.
 */
    inline uint64_t GetBytesTotalSize() const { return m_bytesTotalSize; }

/**
 * Key of the object location in Amazon S3.
 */
    inline const Aws::String& GetKey() const { return m_key; }

/**
 * Upload or Download?
 */
    inline TransferDirection GetTransferDirection() const { return m_direction; }

/**
 * Content type of the object being transferred
 */
    inline const  GetContentType() const { return m_contentType; }

/**
 * The current status of the operation
 */
    TransferStatus GetStatus() const;

/**
 * The last error that was encountered by the transfer. You can handle each error individually via the errorCallback callback function
 * in the TransferConfiguration.
 */
    inline const  GetLastError() const { return m_lastError; }

/**
 * Blocks the calling thread until the operation has finished. This function does not busy wait. It is safe for your CPU.
 */
    void WaitUntilFinished() const;      

    void ApplyDownloadConfiguration(const DownloadConfiguration& downloadConfig);

    bool LockForCompletion() {}

private:
  
    void IsSuccessInCurrentUploadPart(){}
    void GetSuccessPartCount(){ return Uploader.CompletedUploadQueue.size();} 
    void AddToWaitUploadQueue(int PartID){ Uploader.WaitUploadQueue.push_back(PartID);}
    
    //current part status:in progress, in wait
    void SwitchStatus(){}

    void WritePartToDownloadStream(IOStream* partStream, std::size_t writeOffset);

/**
 * Content type of the object being transferred
 */
    inline void SetContentType( value) { m_contentType = value; }

/**
 * The last error that was encountered by the transfer. You can handle each error individually via the errorCallback callback function
 * in the TransferConfiguration.
 */
    inline void SetError(const error) { m_lastError = error; }

/**
* The current status of the operation
*/
    void UpdateStatus(TransferStatus value);

}
