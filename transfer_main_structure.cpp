//do some confiuration before upload/download
class TransferConfig{
    void UploaderConfig(int PartSize){
        // to do in this method
        //1, set PartSize
        //2, calculate FileSize
        //3, calculate PartCount
        //4, Set partID
    }
     // for upload file
     int PartSize	 //specify the part size of the uploading file
}

//do some common operation when upload/download
class FileTransferManager{
public:
    int PartCount;   // Calculated the number of part 
    int PartID;

    //set file path and uploading ObjectName or download object name in the bucket
    void FileTransferManager(string FilePath, string ObjectName, TransferConfig config){}
   
    void DownloaderConfig(int StartPosition,int EndPosition){
        //to do in this method
        //1, set the StartPosition and EndPosition of downloading object 
    }
private:
	// common for upload/download
        int FileSize     
        string FilePath,ObjectName; //specify the path of the upload/download file

        // for download
        int StartPosition,EndPosition; //download the range of the file from StartPosition to EndPosition 
}

//upload file
class Uploader{
    
    static std:queue<int> CompletedUploadQueue,WaitUploadQueue;
    float UploadPartRatio;
    int UploadBufferSize;
    bool PartUploadIsSuccess; //indicate current uploading part is successful or not
    
    int InUploadingPartCount; // In the progress of uploading file, may fail ,success or be canceled in the progress
    void UploadFile(){
        SinglePartUpload();
        MultipartUpload();
    }
    
    void UploadProgressCallback(){}
    void RetryUpload(){}
    void AbortUpload(){
        AbortSingleUpload();
        AbortMultipartUplad();
}
private:
    void HandleUploadPartResponse(){}
    void SinglePartUpload(){}
    void MultiPartUpload(){}

}

//download file
class Downloader{
    float DownloadRatio; //indicate the ratio of downloaded part in the specified range
    int DownloadBufferSize
    void DownloadToDirectory(){}
    void DownloadProgressCallback(){}
}
